﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Inbox.Api.Models;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Inbox.Api.Controllers
{
    /// <summary>
    /// ApiController class
    /// </summary>
    /// <seealso cref="Controller" />
    [Route("/")]
    public class ApiController : Controller
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiController"/> class.
        /// </summary>
        /// <param name="inbox">The inbox.</param>
        /// <param name="logger">The logger.</param>
        /// <param name="token">The token.</param>
        public ApiController(IInbox inbox, ILogger logger, IToken token)
        {
            Inbox = inbox;
            Logger = logger;
            Token = token;
        }

        private IInbox Inbox { get; }

        private ILogger Logger { get; }

        private IToken Token { get; }

        /// <summary>
        /// Sends the specified from.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost("{from}/send/{to}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Send([FromRoute] string from, [FromRoute] string to, [FromBody] SendMessageRequest request)
        {
            try
            {
                await Inbox.Send(request.Parse(from, to));
                return GetStatusCodeResult(204);
            }
            catch (ArgumentException ex)
            {
                Logger.Error(ex.Message, ex);
                return new ErrorResult(400, ex.Message);
            }
        }

        /// <summary>
        /// Gets the messages.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(MessageViewModel[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetMessages()
        {
            try
            {
                var messages = (await Inbox.GetMessages()).Select(MessageViewModel.Parse);
                return Ok(messages);

            }
            catch (ArgumentException ex)
            {
                Logger.Error(ex.Message, ex);
                return new ErrorResult(400, ex.Message);
            }
        }

        /// <summary>
        /// Gets the messages.
        /// </summary>
        /// <returns></returns>
        [HttpGet("/exclude-archive")]
        [ProducesResponseType(typeof(MessageViewModel[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetMessagesWithoutArchive()
        {
            try
            {
                var messages = (await Inbox.GetMessages(true)).Select(MessageViewModel.Parse);
                return Ok(messages);

            }
            catch (ArgumentException ex)
            {
                Logger.Error(ex.Message, ex);
                return new ErrorResult(400, ex.Message);
            }
        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(MessageViewModel), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Read([FromRoute] string id)
        {
            try
            {
                var message = await Inbox.Read(id);
                if (message == null)
                    return new ErrorResult(404, "Message not found");

                return Ok(MessageViewModel.Parse(message));

            }
            catch (ArgumentException ex)
            {
                Logger.Error(ex.Message, ex);
                return new ErrorResult(400, ex.Message);
            }
        }

        /// <summary>
        /// Archives the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPost("{id}/archive")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Archive([FromRoute] string id)
        {
            try
            {
                await Inbox.Archive(id);
                return GetStatusCodeResult(204);
            }
            catch (ArgumentException ex)
            {
                Logger.Error(ex.Message, ex);
                return new ErrorResult(400, ex.Message);
            }
        }

        /// <summary>
        /// Marks as unread.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPost("{id}/unread")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> MarkAsUnread([FromRoute] string id)
        {
            try
            {
                await Inbox.MarkAsUnread(id);
                return GetStatusCodeResult(204);
            }
            catch (ArgumentException ex)
            {
                Logger.Error(ex.Message, ex);
                return new ErrorResult(400, ex.Message);
            }
        }

        /// <summary>
        /// Pins the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPost("{id}/pin")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Pin([FromRoute] string id)
        {
            try
            {
                await Inbox.Pin(id);
                return GetStatusCodeResult(204);
            }
            catch (ArgumentException ex)
            {
                Logger.Error(ex.Message, ex);
                return new ErrorResult(400, ex.Message);
            }
        }

        /// <summary>
        /// Unpins the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPost("{id}/unpin")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Unpin([FromRoute] string id)
        {
            try
            {
                await Inbox.Unpin(id);
                return GetStatusCodeResult(204);
            }
            catch (ArgumentException ex)
            {
                Logger.Error(ex.Message, ex);
                return new ErrorResult(400, ex.Message);
            }
        }

        /// <summary>
        /// Applies the tags.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="tags">The tags.</param>
        /// <returns></returns>
        [HttpPost("{id}/tags")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> ApplyTags([FromRoute] string id, string[] tags)
        {
            try
            {
                await Inbox.ApplyTags(id, tags);
                return GetStatusCodeResult(204);
            }
            catch (ArgumentException ex)
            {
                Logger.Error(ex.Message, ex);
                return new ErrorResult(400, ex.Message);
            }
        }

        /// <summary>
        /// Removes the tags.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="tags">The tags.</param>
        /// <returns></returns>
        [HttpDelete("{id}/tags")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> RemoveTags([FromRoute] string id, string[] tags)
        {
            try
            {
                await Inbox.RemoveTags(id, tags);
                return GetStatusCodeResult(204);
            }
            catch (ArgumentException ex)
            {
                Logger.Error(ex.Message, ex);
                return new ErrorResult(400, ex.Message);
            }
        }

        private IActionResult GetStatusCodeResult(int statusCode)
        {
#if DOTNET2
            return new StatusCodeResult(statusCode);
#else
            return new HttpStatusCodeResult(statusCode);
#endif
        }
    }
}