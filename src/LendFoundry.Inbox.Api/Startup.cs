﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Inbox.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Inbox.Configuration;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Foundation.Persistence.Mongo;
using System.Collections.Generic;
using LendFoundry.Inbox.Engine;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.AssignmentEngine.Client;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services.Handlebars;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif
namespace LendFoundry.Inbox.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // Register the Swagger generator, defining one or more Swagger documents
           #if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "Inbox"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme() 
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> 
                { 
					{ "Bearer", new string[]{} }
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.Inbox.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif

            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddTenantService();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddConfigurationService<InboxConfiguration>(Settings.ServiceName);
            services.AddEventHub(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<InboxConfiguration>(Settings.ServiceName);
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddLookupService();
            services.AddDecisionEngine();
            services.AddAssignmentService();
            services.AddIdentityService();


            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();

            services.AddTransient(p => p.GetRequiredService<ITokenHandler>().Parse());
            services.AddTransient<IMessageRepository, MessageRepository>();
            services.AddTransient<IMessageRepositoryFactory, MessageRepositoryFactory>();
            services.AddTransient<IInbox, Engine.Inbox>();
            services.AddTransient<IInboxListener, InboxListener>();
            services.AddTransient<IInboxFactory, InboxFactory>();
            services.AddTransient<IHandlebarsResolver, HandlebarsResolver>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);

            // Enable middleware to serve generated Swagger as a JSON endpoint.
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Inbox Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif

            
            app.UseErrorHandling();
            app.UseInboxListener();
            app.UseRequestLogging();            
            app.UseMvc();
            app.UseHandlebars();
            app.UseConfigurationCacheDependency();
        }
    }
}