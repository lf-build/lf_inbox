using System;

namespace LendFoundry.Inbox.Api.Models
{
    /// <summary>
    /// MessageViewModel
    /// </summary>
    public class MessageViewModel : IMessage
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        public DateTimeOffset Date { get; set; }

        /// <summary>
        /// From
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// To
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// Subject
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Body
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Tags
        /// </summary>
        public string[] Tags { get; set; }

        /// <summary>
        /// IsRead
        /// </summary>
        public bool IsRead { get; set; }

        /// <summary>
        /// IsPinned
        /// </summary>
        public bool IsPinned { get; set; }

        /// <summary>
        /// IsArchived
        /// </summary>
        public bool IsArchived { get; set; }

        /// <summary>
        /// TenantId
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Parse method
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static MessageViewModel Parse(IMessage message)
        {
            return new MessageViewModel
            {
                Id = message.Id,
                Date = message.Date,
                From = message.From,
                To = message.To,
                Subject = message.Subject,
                Body = message.Body,
                Tags = message.Tags,
                IsArchived = message.IsArchived,
                IsPinned = message.IsPinned,
                IsRead = message.IsRead,
                TenantId = message.TenantId
            };
        }
    }
}