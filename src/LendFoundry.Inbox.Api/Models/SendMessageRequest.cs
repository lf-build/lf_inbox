using System;
using LendFoundry.Inbox.Engine;

namespace LendFoundry.Inbox.Api.Models
{
    /// <summary>
    /// SendMessageRequest
    /// </summary>
    public class SendMessageRequest
    {
        /// <summary>
        /// Subject
        /// </summary>
        public string Subject { get; set; }
        /// <summary>
        /// Body
        /// </summary>
        public string Body { get; set; }
        /// <summary>
        /// Tags
        /// </summary>
        public string[] Tags { get; set; }
        /// <summary>
        /// Parse method
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public IMessage Parse(string from, string to)
        {
            return new Message
            {
                From = from,
                To = to,
                Subject = Subject,
                Body = Body,
                Tags = Tags
            };
        }
    }
}