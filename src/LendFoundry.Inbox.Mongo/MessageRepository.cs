﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace LendFoundry.Inbox.Mongo
{
    public class MessageRepository : MongoRepository<IMessage, Message>, IMessageRepository
    {
        static MessageRepository()
        {
            BsonClassMap.RegisterClassMap<Message>(map =>
            {
                map.AutoMap();
                var type = typeof(Message);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public MessageRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "inbox")
        {

        }

        public async Task AddMany(string tenantId, List<IMessage> messages)
        {
            if (string.IsNullOrEmpty(tenantId))
                throw new ArgumentNullException(nameof(tenantId));

            if (messages == null || !messages.Any())
                throw new ArgumentNullException(nameof(messages));

            await Collection.InsertManyAsync(messages);
        }

        public async Task Update(string tenantId, string username, IMessage message)
        {
            if (string.IsNullOrEmpty(tenantId))
                throw new ArgumentNullException(nameof(tenantId));

            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            if (message == null)
                throw new ArgumentNullException(nameof(message));

            await Collection.ReplaceOneAsync(m => m.TenantId == tenantId && m.To == username && m.Id == message.Id, message);
        }

        public async Task<IEnumerable<IMessage>> GetAll(string tenantId, string username, bool excludeArchive = false)
        {
            if (string.IsNullOrEmpty(tenantId))
                throw new ArgumentNullException(nameof(tenantId));

            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            return await Task.Run(() =>
            {
                if (excludeArchive)
                {
                    return Collection.AsQueryable()
                    .Where(m => m.TenantId == tenantId && m.To == username && !m.IsArchived)
                    .OrderByDescending(m => m.IsPinned)
                    .ThenByDescending(m => m.Date)
                    .AsEnumerable();
                }
                return Collection.AsQueryable()
                    .Where(m => m.TenantId == tenantId && m.To == username)
                    .OrderByDescending(m => m.IsPinned)
                    .ThenByDescending(m => m.Date)
                    .AsEnumerable();
            });
        }

        public async Task<IMessage> Get(string tenantId, string username, string messageId)
        {
            if (string.IsNullOrEmpty(tenantId))
                throw new ArgumentNullException(nameof(tenantId));

            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            if (string.IsNullOrEmpty(messageId))
                throw new ArgumentNullException(nameof(messageId));

            return await Task.Run(() => Collection.AsQueryable().FirstOrDefault(m => m.TenantId == tenantId && m.To == username && m.Id == messageId));
        }
    }
}
