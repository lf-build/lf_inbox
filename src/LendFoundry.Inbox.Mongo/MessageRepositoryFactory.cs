﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Inbox.Mongo
{
    public class MessageRepositoryFactory : IMessageRepositoryFactory
    {
        public MessageRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IMessageRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfigurationFactory = Provider.GetService<IMongoConfigurationFactory>();
            var configuration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());
            return new MessageRepository(tenantService, configuration);
        }
    }
}
