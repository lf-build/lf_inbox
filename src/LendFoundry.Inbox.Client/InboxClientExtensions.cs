﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Inbox.Client
{
    public static class InboxClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddInbox(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IInboxClientFactory>(p => new InboxClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IInboxClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddInbox(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IInboxClientFactory>(p => new InboxClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IInboxClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddInbox(this IServiceCollection services)
        {
            services.AddTransient<IInboxClientFactory>(p => new InboxClientFactory(p));
            services.AddTransient(p => p.GetService<IInboxClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}