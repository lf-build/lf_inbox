﻿using System.Threading.Tasks;

namespace LendFoundry.Inbox.Client
{
    public interface IInboxClient
    {
        Task Send(string from, string to, string subject, string body, string[] tags);
    }
}