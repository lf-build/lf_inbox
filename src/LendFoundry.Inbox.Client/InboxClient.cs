﻿using System;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using RestSharp;

namespace LendFoundry.Inbox.Client
{
    public class InboxClient : IInboxClient
    {
        public InboxClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }


        public async Task Send(string from, string to, string subject, string body, string[] tags)
        {
            if (string.IsNullOrEmpty(from))
                throw new ArgumentNullException(nameof(from));

            if (string.IsNullOrEmpty(to))
                throw new ArgumentNullException(nameof(to));

            if (string.IsNullOrEmpty(subject))
                throw new ArgumentNullException(nameof(subject));

            if (string.IsNullOrEmpty(body))
                throw new ArgumentNullException(nameof(body));

            var request = new RestRequest("/{from}/send/{to}", Method.POST);
            request.AddUrlSegment("from", from);
            request.AddUrlSegment("to", to);
            request.AddJsonBody(new Message
            {
                Subject = subject,
                Body = body,
                Tags = tags
            });

            await Client.ExecuteAsync(request);
        }
    }
}
