using LendFoundry.Security.Tokens;

namespace LendFoundry.Inbox.Client
{
    public interface IInboxClientFactory
    {
        IInboxClient Create(ITokenReader reader);
    }
}