﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Inbox.Engine
{
    public static class InboxListenerExtensions
    {
        public static void UseInboxListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IInboxListener>().Start();
        }
    }
}
