﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Tenant.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.AssignmentEngine.Client;
using LendFoundry.Security.Identity.Client;

namespace LendFoundry.Inbox.Engine
{
    public class InboxFactory : IInboxFactory
    {
        public InboxFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IInbox Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        {
            var messageRepositoryFactory = Provider.GetService<IMessageRepositoryFactory>();
            var messageRepository = messageRepositoryFactory.Create(reader);

            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var lookupServiceFactory = Provider.GetService<ILookupClientFactory>();
            var lookupService = lookupServiceFactory.Create(reader);

            var decisionEngineServiceFactory = Provider.GetService<IDecisionEngineClientFactory>();
            var decisionEngineService = decisionEngineServiceFactory.Create(reader);

            var assignmentServiceFactory = Provider.GetService<IAssignmentServiceClientFactory>();
            var assignmentService = assignmentServiceFactory.Create(reader);

            var identityServiceFactory = Provider.GetService<IIdentityServiceFactory>();
            var identityService = identityServiceFactory.Create(reader);

            return new Inbox(messageRepository, tenantService, handler, reader, eventHub, logger, tenantTime, decisionEngineService, identityService, assignmentService);
        }

    }
}
