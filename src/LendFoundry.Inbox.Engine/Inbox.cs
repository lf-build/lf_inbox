﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System.Linq;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Date;
using LendFoundry.Inbox.Configuration;
using Newtonsoft.Json.Linq;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Security.Identity.Client;
using LendFoundry.AssignmentEngine;

namespace LendFoundry.Inbox.Engine
{
    public class Inbox : IInbox
    {
        #region Constructor

        //TODO: encapsulate session info from tokenHandler
        public Inbox(IMessageRepository repository,
                    ITenantService tenantService,
                    ITokenHandler tokenHandler,
                    ITokenReader tokenReader,
                    IEventHubClient eventHub,
                    ILogger logger,
                    ITenantTime tenantTime,
                    IDecisionEngineService decisionEngineService,
                    IIdentityService identityService,
                    IAssignmentService assignmentService)
        {
            Repository = repository;
            TokenReader = tokenReader;
            TokenHandler = tokenHandler;
            Tenant = tenantService.Current;
            EventHub = eventHub;
            Logger = logger;
            TenantTime = tenantTime;
            DecisionEngineService = decisionEngineService;
            IdentityService = identityService;
            AssignmentService = assignmentService;
        }

        #endregion

        #region Variables

        private IMessageRepository Repository { get; }
        private ITokenHandler TokenHandler { get; }
        private ITokenReader TokenReader { get; }
        private TenantInfo Tenant { get; }
        private IEventHubClient EventHub { get; }
        private ILogger Logger { get; }
        private ITenantTime TenantTime { get; }
        private IDecisionEngineService DecisionEngineService { get; }
        private IIdentityService IdentityService { get; }
        private IAssignmentService AssignmentService { get; }

        #endregion

        #region Public Methods

        //TODO: validate if users exist in our system for the given tenant
        public async Task Send(IMessage message)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            if (string.IsNullOrEmpty(message.From))
                throw new ArgumentException("Who is sending the message is not specified.");

            if (string.IsNullOrEmpty(message.To))
                throw new ArgumentException("Who is receiving the message is not specified.");

            message.Date = TenantTime.Now;

            await Task.Run(() =>
            {
                Repository.Add(message);
            });

        }

        public async Task<IEnumerable<IMessage>> GetMessages(bool excludeArchive = false)
        {
            return await Repository.GetAll(Tenant.Id, GetSubjectFromToken(), excludeArchive);
        }

        public async Task<IMessage> Read(string id)
        {
            ValidateMessageId(id);
            var subject = GetSubjectFromToken();
            var message = await Repository.Get(Tenant.Id, subject, id);
            EnsureMessageIsFound(message);
            message.IsRead = true;

            await Repository.Update(Tenant.Id, subject, message);
            await EventHub.Publish("InboxMessageRead", new MessageEvent(message));
            return message;
        }

        public async Task Archive(string id)
        {
            ValidateMessageId(id);
            var subject = GetSubjectFromToken();
            var message = await Repository.Get(Tenant.Id, subject, id);
            message.IsArchived = true;
            await Repository.Update(Tenant.Id, subject, message);
            await EventHub.Publish("InboxMessageArchived", new MessageEvent(message));
        }

        public async Task MarkAsUnread(string id)
        {
            ValidateMessageId(id);
            var subject = GetSubjectFromToken();
            var message = await Repository.Get(Tenant.Id, subject, id);

            EnsureMessageIsFound(message);

            message.IsRead = false;
            await Repository.Update(Tenant.Id, subject, message);
            await EventHub.Publish("InboxMessageMarkedAsUnread", new MessageEvent(message));
        }

        public async Task Pin(string id)
        {
            ValidateMessageId(id);
            var subject = GetSubjectFromToken();
            var message = await Repository.Get(Tenant.Id, subject, id);

            EnsureMessageIsFound(message);

            message.IsPinned = true;
            await Repository.Update(Tenant.Id, subject, message);
            await EventHub.Publish("InboxMessagePinned", new MessageEvent(message));
        }

        public async Task Unpin(string id)
        {
            ValidateMessageId(id);
            var subject = GetSubjectFromToken();
            var message = await Repository.Get(Tenant.Id, subject, id);

            EnsureMessageIsFound(message);

            message.IsPinned = false;
            await Repository.Update(Tenant.Id, subject, message);
            await EventHub.Publish("InboxMessageUnpinned", new MessageEvent(message));
        }

        public async Task ApplyTags(string id, string[] tags)
        {
            ValidateMessageId(id);
            ValidateTags(tags);
            var subject = GetSubjectFromToken();
            var message = await Repository.Get(Tenant.Id, subject, id);

            EnsureMessageIsFound(message);

            message.Tags = tags.Concat(message.Tags).Distinct().ToArray();

            await Repository.Update(Tenant.Id, subject, message);
            await EventHub.Publish("InboxMessageTagsApplied", new TagEvent(message, tags));
        }

        public async Task RemoveTags(string id, string[] tags)
        {
            ValidateMessageId(id);
            ValidateTags(tags);
            var subject = GetSubjectFromToken();
            var message = await Repository.Get(Tenant.Id, subject, id);

            EnsureMessageIsFound(message);

            message.Tags = message.Tags.Except(tags).ToArray();

            await Repository.Update(Tenant.Id, subject, message);
            await EventHub.Publish("InboxMessageTagsRemoved", new TagEvent(message, tags));
        }

        public Action<EventInfo> ProcessInbox(InboxConfiguration inboxConfiguration, Dictionary<string, string> lookupEntries)
        {
            return @event =>
           {
               try
               {
                   Logger.Info($"Processing {@event.Name} for {@event.TenantId} with Id - {@event.Id} ");
                   if (inboxConfiguration == null)
                       throw new ArgumentException($"The configuration for service #{Settings.ServiceName} could not be found, please verify");

                   if (inboxConfiguration.Entities == null || !inboxConfiguration.Entities.Any())
                       throw new ArgumentException($"The configuration related to Entities could not be found, please verify");

                   var data = JObject.FromObject(@event.Data).ToObject<JObject>();

                   var configurations = new Dictionary<string, IEnumerable<EventConfiguration>>();
                   inboxConfiguration.Entities.Where(entity => entity.Value.Any(ev => ev.EventName.Equals(@event.Name, StringComparison.OrdinalIgnoreCase)))
                       .Select(e =>
                       {
                           configurations.Add(e.Key, e.Value.Where(ev => ev.EventName.Equals(@event.Name, StringComparison.OrdinalIgnoreCase)).ToList());
                           return e;
                       }).ToList();

                   foreach (var keyValue in configurations)
                   {
                       var entityType = ValidateEntityType(keyValue.Key, lookupEntries);
                       keyValue.Value.ToList().ForEach(configuration =>
                       {
                           ProcessInboxMessage(@event, configuration, data, keyValue).Wait();
                       });
                   }
               }
               catch (Exception ex)
               {
                   Logger.Error($"Error while processing event #{@event.Name}. Error: {ex.Message}", ex, @event);
                   Logger.Info("Service is working yet and waiting new event");
               }
           };
        }

        #endregion

        #region Private Methods

        private static void ValidateMessageId(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException(nameof(id));
        }

        private static void ValidateTags(IEnumerable<string> tags)
        {
            if (tags == null || !tags.Any())
                throw new ArgumentException("You need to have at least a tag.", nameof(tags));
        }

        private static void EnsureMessageIsFound(IMessage message)
        {
            if (message == null)
                throw new NotFoundException("Message not found.");
        }

        private string GetSubjectFromToken()
        {
            var token = TokenHandler.Parse(TokenReader.Read());
            if (token != null)
            {
                return token.Subject;
            }
            return string.Empty;
        }

        private string ValidateEntityType(string entityType, Dictionary<string, string> lookups)
        {
            entityType = entityType.ToLower();
            if (!lookups.ContainsKey(entityType))
                throw new InvalidArgumentException($"Invalid entityType {entityType}", nameof(entityType));
            return entityType;
        }

        private IMessage Parse(EventInfo @event, EventConfiguration configuration, JObject data)
        {
            @event.Data = JObject.FromObject(@event.Data);

            var subject = "";
            if (configuration.Subject.IndexOf("{") >= 0)
                subject = configuration.Subject.FormatWith(@event);
            else
                subject = configuration.Subject;

            var description = "";
            if (configuration.Description.IndexOf("{") >= 0)
                description = configuration.Description.FormatWith(@event);
            else
                description = configuration.Description;

            var message = new Message
            {
                Body = description,
                Date = TenantTime.Now,
                From = "system",
                Subject = subject,
                Tags = configuration.Tags,
                TenantId = Tenant.Id
            };

            return message;
        }

        private async Task ProcessInboxMessage(EventInfo @event, EventConfiguration configuration, JObject data, KeyValuePair<string, IEnumerable<EventConfiguration>> keyValue)
        {
            var entityId = Convert.ToString(InterpolateExtentions.GetPropertyValue(configuration.EntityId.Replace("{", "").Replace("}", ""), @event));
            var entityType = configuration.EntityType.FormatWith(@event);

            if (entityType != keyValue.Key)
                return;

            var messageUsers = new List<string>();

            switch (configuration.NotifyTo)
            {
                case NotifyTo.All:
                    var users = await IdentityService.GetAllUsers();
                    if (users == null || !users.Any())
                    {
                        Logger.Warn($"No users found for sending message with event {@event.Name}");
                        return;
                    }
                    else
                        messageUsers.AddRange(users.Select(u => u.Username));
                    break;
                case NotifyTo.Assigned:
                    var assignedUsers = await AssignmentService.Get(entityType, entityId);
                    if (assignedUsers == null || !assignedUsers.Any())
                    {
                        Logger.Warn($"No assigned users found for sending message with event {@event.Name}");
                        return;
                    }
                    else
                        messageUsers.AddRange(assignedUsers.Select(u => u.Assignee));
                    break;
                case NotifyTo.Rule:
                    if (configuration.NotifyToSetting == null || string.IsNullOrWhiteSpace(configuration.NotifyToSetting.RuleName))
                    {
                        Logger.Error($"Rule name not specified for event {@event.Name} in configuration");
                        return;
                    }
                    try
                    {
                        var ruleUsers = DecisionEngineService.Execute<dynamic, List<string>>(configuration.NotifyToSetting.RuleName, data);
                        if (ruleUsers == null || !ruleUsers.Any())
                        {
                            Logger.Warn($"No rule users found for sending message with event {@event.Name}");
                            return;
                        }
                        else
                            messageUsers.AddRange(ruleUsers);

                    }
                    catch (Exception ex)
                    {
                        Logger.Error($"Error while processing rule {configuration.NotifyToSetting.RuleName}.", ex);
                    }
                    break;
                case NotifyTo.Role:
                    if (configuration.NotifyToSetting == null || configuration.NotifyToSetting.Roles == null || !configuration.NotifyToSetting.Roles.Any())
                    {
                        Logger.Error($"Roles not specified for event {@event.Name} in configuration");
                        return;
                    }
                    var subRoles = await IdentityService.GetChildRoles(configuration.NotifyToSetting.Roles);
                    var roles = configuration.NotifyToSetting.Roles;
                    if (subRoles != null && subRoles.Any())
                        roles.AddRange(subRoles);
                    var roleUsers = await IdentityService.GetUsersByRoles(roles);
                    if (roleUsers == null || !roleUsers.Any())
                    {
                        Logger.Warn($"No rule users found for sending message with event {@event.Name}");
                        return;
                    }
                    else
                        messageUsers.AddRange(roleUsers.Select(u => u.Username));
                    break;
                default:
                    Logger.Error($"Invalid value of NotifyTo for event configuration {@event.Name}");
                    break;
            }

            if (messageUsers.Any())
            {
                var message = Parse(@event, configuration, data);
                var messages = new List<IMessage>();
                foreach (var user in messageUsers)
                {
                    var userMessage = new Message(message, user);
                    messages.Add(userMessage);
                }

                await Repository.AddMany(Tenant.Id, messages);
            }
        }

        #endregion

    }
}
