﻿using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Inbox.Engine
{
    public class TagEvent : MessageEvent
    {
        public TagEvent(IMessage message, IEnumerable<string> tags) : base(message)
        {
            Tags = tags.ToArray();
        }

        public string[] Tags { get; set; }
    }
}