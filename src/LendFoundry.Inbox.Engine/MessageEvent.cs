﻿namespace LendFoundry.Inbox.Engine
{
    public class MessageEvent
    {
        public MessageEvent(IMessage message)
        {
            Message = message;
        }

        public IMessage Message { get; set; }
    }
}