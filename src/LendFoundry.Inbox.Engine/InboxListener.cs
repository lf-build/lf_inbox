﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Listener;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Inbox.Configuration;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Inbox.Engine
{
    public class InboxListener : ListenerBase, IInboxListener
    {
        public InboxListener(IConfigurationServiceFactory configurationFactory, 
                            IInboxFactory inboxFactory, 
                            ITokenHandler tokenHandler, 
                            IEventHubClientFactory eventHubFactory, 
                            ILoggerFactory loggerFactory, 
                            ITenantServiceFactory tenantServiceFactory,
                            ILookupClientFactory lookupServiceFactory) : base(tokenHandler, eventHubFactory, loggerFactory, tenantServiceFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            EventHubFactory = eventHubFactory;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            InboxFactory = inboxFactory;
            LookupServiceFactory = lookupServiceFactory;
        }

        private ITokenHandler TokenHandler { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ILoggerFactory LoggerFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IInboxFactory InboxFactory { get; }
        private ILookupClientFactory LookupServiceFactory { get; }

        public override List<string> SubscribeEvents(string tenant, ILogger logger)
        {
            try
            {
                var token = TokenHandler.Issue(tenant, Settings.ServiceName);
                var reader = new StaticTokenReader(token.Value);
                var inboxService = InboxFactory.Create(reader, TokenHandler, logger);
                var eventhub = EventHubFactory.Create(reader);
                var lookupService = LookupServiceFactory.Create(reader);
                var configurationService = ConfigurationFactory.Create<InboxConfiguration>(Settings.ServiceName, reader);
                configurationService.ClearCache(Settings.ServiceName);
                var configuration = configurationService.Get();
                if (configuration == null)
                {
                    logger.Error($"The configuration for service #{Settings.ServiceName} could not be found for {tenant} , please verify");
                    return null;
                }
                else
                {
                    logger.Info($"#{configuration.Entities.Keys.Count} entity(ies) found from configuration: {Settings.ServiceName}");

                    var uniqueEvents = configuration.Entities
                        .SelectMany(ent => ent.Value.Select(c => c.EventName))
                        .Distinct()
                        .ToList();

                    var lookupEntries = lookupService.GetLookupEntries("entityTypes");
                    uniqueEvents.ForEach(eventName =>
                    {
                        eventhub.On(eventName, inboxService.ProcessInbox(configuration, lookupEntries));
                        logger.Info($"It was made subscription to EventHub with the Event: #{eventName} for tenant {tenant}");
                    });
                    return uniqueEvents;
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Unable to subscribe event for ${tenant}", ex);
                return new List<string>();
            }
        }

        public override List<string> GetEventNames(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var configurationService = ConfigurationFactory.Create<InboxConfiguration>(Settings.ServiceName, reader);
            configurationService.ClearCache(Settings.ServiceName);
            var configuration = configurationService.Get();
            if (configuration == null)
            {
                return null;
            }
            else
            {
                return configuration.Entities.SelectMany(ent => ent.Value.Select(c => c.EventName)).Distinct().ToList();
            }
        }
    }
}
