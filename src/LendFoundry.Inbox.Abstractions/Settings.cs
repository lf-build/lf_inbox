using System;

namespace LendFoundry.Inbox
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "inbox";
    }
}