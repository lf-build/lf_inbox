using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Inbox
{
    public interface IMessageRepository : IRepository<IMessage>
    {
        Task AddMany(string tenantId, List<IMessage> messages);

        Task Update(string tenantId, string username, IMessage message);

        Task<IEnumerable<IMessage>> GetAll(string tenantId, string username, bool excludeArchive = false);

        Task<IMessage> Get(string tenantId, string username, string messageId);
    }
}