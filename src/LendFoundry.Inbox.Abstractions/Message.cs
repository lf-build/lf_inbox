﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Inbox
{
    public class Message : Aggregate, IMessage
    {
        public Message()
        {
        }

        public Message(IMessage message, string toAddress)
        {
            Body = message.Body;
            Date = message.Date;
            From = message.From;
            Subject = message.Subject;
            Tags = message.Tags;
            To = toAddress;
            TenantId = message.TenantId;
        }

        public DateTimeOffset Date { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public string[] Tags { get; set; }

        public bool IsRead { get; set; }

        public bool IsPinned { get; set; }

        public bool IsArchived { get; set; }
    }
}