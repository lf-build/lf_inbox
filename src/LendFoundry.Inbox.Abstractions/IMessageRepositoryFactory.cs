﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Inbox
{
    public interface IMessageRepositoryFactory
    {
        IMessageRepository Create(ITokenReader reader);
    }
}