﻿namespace LendFoundry.Inbox
{
    public interface IInboxListener
    {
        void Start();
    }
}
