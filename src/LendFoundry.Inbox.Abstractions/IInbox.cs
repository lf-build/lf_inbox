﻿using LendFoundry.EventHub;
using LendFoundry.Inbox.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Inbox
{
    public interface IInbox
    {
        Task Send(IMessage message);

        Task<IEnumerable<IMessage>> GetMessages(bool excludeArchive = false);

        Task<IMessage> Read(string id);

        Task Archive(string id);

        Task MarkAsUnread(string id);

        Task Pin(string id);

        Task Unpin(string id);

        Task ApplyTags(string id, string[] tags);

        Task RemoveTags(string id, string[] tags);

        Action<EventInfo> ProcessInbox(InboxConfiguration inboxConfiguration, Dictionary<string, string> lookupEntries);
    }


}