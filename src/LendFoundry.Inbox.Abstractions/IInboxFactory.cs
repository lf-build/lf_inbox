﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Inbox
{
    public interface IInboxFactory
    {
        IInbox Create(ITokenReader reader, ITokenHandler handler, ILogger logger);
    }
}
