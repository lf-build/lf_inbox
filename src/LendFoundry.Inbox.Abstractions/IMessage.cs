﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Inbox
{
    public interface IMessage : IAggregate
    {

        DateTimeOffset Date { get; set; }

        string From { get; set; }

        string To { get; set; }

        string Subject { get; set; }

        string Body { get; set; }

        string[] Tags { get; set; }

        bool IsRead { get; set; }

        bool IsPinned { get; set; }

        bool IsArchived { get; set; }
    }
}
