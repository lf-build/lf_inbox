﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Inbox.Configuration
{
    public class EventConfiguration
    {
        public string EventName { get; set; }
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string[] Tags { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public NotifyTo NotifyTo { get; set; }
        public NotifyToSetting NotifyToSetting { get; set; }
    }
}
