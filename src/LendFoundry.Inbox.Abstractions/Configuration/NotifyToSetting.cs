﻿using System.Collections.Generic;

namespace LendFoundry.Inbox.Configuration
{
    public class NotifyToSetting
    {
        public string RuleName { get; set; }
        public List<string> Roles { get; set; }
    }
}