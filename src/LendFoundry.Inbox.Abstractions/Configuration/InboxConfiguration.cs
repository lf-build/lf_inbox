using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Inbox.Configuration
{
    public class InboxConfiguration : IDependencyConfiguration
    {
        public Dictionary<string, IEnumerable<EventConfiguration>> Entities { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}