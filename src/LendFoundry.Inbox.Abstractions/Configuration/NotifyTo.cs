﻿namespace LendFoundry.Inbox.Configuration
{
    public enum NotifyTo
    {
        All,
        Assigned,
        Rule,
        Role
    }
}
