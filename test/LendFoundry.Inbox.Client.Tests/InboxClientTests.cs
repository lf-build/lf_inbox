﻿using LendFoundry.Foundation.Client;
using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace LendFoundry.Inbox.Client.Tests
{
    public class   InboxClientTests
    {
        [Fact]
        public void Client_Send()
        {
            IRestRequest request = null;
            var mockServiceClient = new Mock<IServiceClient>();
            mockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                 .Callback<IRestRequest>(r => request = r)
                ;

            var inboxClient = new InboxClient(mockServiceClient.Object);

            var result = inboxClient.Send("kinjal.s@sigmainfo.net", "kinjal.s@sigmainfo.net","send","send", new string[] { "tag1","tag2" });

            Assert.NotNull(result);
            Assert.NotNull(request);
            Assert.Equal("/{from}/send/{to}", request.Resource);
            Assert.Equal(Method.POST, request.Method);
        }
    }
}
