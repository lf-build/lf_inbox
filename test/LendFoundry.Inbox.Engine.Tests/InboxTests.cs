﻿using LendFoundry.EventHub;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Moq;
using System;
using Xunit;
using System.Linq;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Date;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Security.Identity.Client;
using LendFoundry.AssignmentEngine;

namespace LendFoundry.Inbox.Engine.Tests
{
    public class InboxTests
    {
        [Fact]
        public void SendThrownArgumentNullExceptionWhenMessageNull()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = new Inbox(new FakeMessageRepository(), Mock.Of<ITenantService>(), Mock.Of<ITokenHandler>(), Mock.Of<ITokenReader>(), Mock.Of<IEventHubClient>(), Mock.Of<ILogger>(), Mock.Of<ITenantTime>(), Mock.Of<IDecisionEngineService>(), Mock.Of<IIdentityService>(), Mock.Of<IAssignmentService>());
                await service.Send(null);
            });
        }

        [Fact]
        public void SendThrownArgumentNullExceptionWhenMessageWithFormNull()
        {
            Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                var service = new Inbox(new FakeMessageRepository(), Mock.Of<ITenantService>(), Mock.Of<ITokenHandler>(), Mock.Of<ITokenReader>(), Mock.Of<IEventHubClient>(), Mock.Of<ILogger>(), Mock.Of<ITenantTime>(), Mock.Of<IDecisionEngineService>(), Mock.Of<IIdentityService>(), Mock.Of<IAssignmentService>());
                await service.Send(new Message { To = "kinjal.s@sigmainfo.net" });
            });
        }

        [Fact]
        public void SendThrownArgumentNullExceptionWhenMessageWithToNull()
        {
            Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                var service = new Inbox(new FakeMessageRepository(), Mock.Of<ITenantService>(), Mock.Of<ITokenHandler>(), Mock.Of<ITokenReader>(), Mock.Of<IEventHubClient>(), Mock.Of<ILogger>(), Mock.Of<ITenantTime>(), Mock.Of<IDecisionEngineService>(), Mock.Of<IIdentityService>(), Mock.Of<IAssignmentService>());
                await service.Send(new Message { From = "kinjal.s@sigmainfo.net" });
            });
        }

        [Fact]
        public async void SendMessageWithSuccess()
        {
            var service = GetService();
            await service.Send(new Message { To = "kinjal.s@sigmainfo.net", From = "kinjal.s@sigmainfo.net" });

            var result = await service.GetMessages();

            Assert.NotNull(result);
            Assert.True(result.Count() > 0);
        }

        [Fact]
        public async void GetMessagesWithSuccess()
        {
            var service = GetService();
            await service.Send(new Message { To = "kinjal.s@sigmainfo.net", From = "kinjal.s@sigmainfo.net" });

            var result = await service.GetMessages();

            Assert.NotNull(result);
            Assert.True(result.Count() > 0);
        }

        [Fact]
        public void ReadThrowsArgumentNullExceptionWhenIdNullOrEmpty()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = GetService();
                await service.Read(null);
            });


        }
        [Fact]
        public void ReadThrowsNotFoundExceptionWhenIdNotExist()
        {
            Assert.ThrowsAsync<NotFoundException>(async () =>
            {
                var service = GetService();
                await service.Read("invalid");
            });


        }

        [Fact]
        public async void ReadWhenIdSuccess()
        {
            var service = GetService();
            await service.Send(new Message { To = "kinjal.s@sigmainfo.net", From = "kinjal.s@sigmainfo.net" });

            var result = await service.GetMessages();
            var firstMessage = result.First();

            var messgeResult = await service.Read(firstMessage.Id);
            Assert.NotNull(messgeResult);
            Assert.True(messgeResult.IsRead);

        }



        [Fact]
        public void ArchiveThrowsArgumentNullExceptionWhenIdNullOrEmpty()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = GetService();
                await service.Archive(null);
            });


        }
        [Fact]
        public void ArchiveThrowsNotFoundExceptionWhenIdNotExist()
        {
            Assert.ThrowsAsync<NotFoundException>(async () =>
            {
                var service = GetService();
                await service.Archive("invalid");
            });
        }

        [Fact]
        public async void ArchiveWhenIdSuccess()
        {
            var service = GetService();
            await service.Send(new Message { To = "kinjal.s@sigmainfo.net", From = "kinjal.s@sigmainfo.net" });

            var result = await service.GetMessages();
            var firstMessage = result.First();

            await service.Archive(firstMessage.Id);
            result = await service.GetMessages();
            var updatedMessage = result.First();
            Assert.NotNull(updatedMessage);
            Assert.True(updatedMessage.IsArchived);

        }



        [Fact]
        public void MarkAsUnreadThrowsArgumentNullExceptionWhenIdNullOrEmpty()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = GetService();
                await service.MarkAsUnread(null);
            });


        }
        [Fact]
        public void MarkAsUnreadThrowsNotFoundExceptionWhenIdNotExist()
        {
            Assert.ThrowsAsync<NotFoundException>(async () =>
            {
                var service = GetService();
                await service.MarkAsUnread("invalid");
            });


        }

        [Fact]
        public async void MarkAsUnreadWhenIdSuccess()
        {
            var service = GetService();
            await service.Send(new Message { To = "kinjal.s@sigmainfo.net", From = "kinjal.s@sigmainfo.net" });

            var result = await service.GetMessages();
            var firstMessage = result.First();

            await service.MarkAsUnread(firstMessage.Id);
            result = await service.GetMessages();
            var updatedMessage = result.First();
            Assert.NotNull(updatedMessage);
            Assert.False(updatedMessage.IsRead);

        }



        [Fact]
        public void PinThrowsArgumentNullExceptionWhenIdNullOrEmpty()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = GetService();
                await service.Pin(null);
            });


        }
        [Fact]
        public void PinThrowsNotFoundExceptionWhenIdNotExist()
        {
            Assert.ThrowsAsync<NotFoundException>(async () =>
            {
                var service = GetService();
                await service.Pin("invalid");
            });


        }

        [Fact]
        public async void PinWhenIdSuccess()
        {
            var service = GetService();
            await service.Send(new Message { To = "kinjal.s@sigmainfo.net", From = "kinjal.s@sigmainfo.net" });

            var result = await service.GetMessages();
            var firstMessage = result.First();

            await service.Pin(firstMessage.Id);
            result = await service.GetMessages();
            var updatedMessage = result.First();
            Assert.NotNull(updatedMessage);
            Assert.True(updatedMessage.IsPinned);

        }



        [Fact]
        public void UnpinThrowsArgumentNullExceptionWhenIdNullOrEmpty()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = GetService();
                await service.Unpin(null);
            });


        }
        [Fact]
        public void UnpinThrowsNotFoundExceptionWhenIdNotExist()
        {
            Assert.ThrowsAsync<NotFoundException>(async () =>
            {
                var service = GetService();
                await service.Unpin("invalid");
            });


        }

        [Fact]
        public async void UnpinWhenIdSuccess()
        {
            var service = GetService();
            await service.Send(new Message { To = "kinjal.s@sigmainfo.net", From = "kinjal.s@sigmainfo.net" });

            var result = await service.GetMessages();
            var firstMessage = result.First();

            await service.Unpin(firstMessage.Id);
            result = await service.GetMessages();
            var updatedMessage = result.First();
            Assert.NotNull(updatedMessage);
            Assert.False(updatedMessage.IsPinned);

        }



        [Fact]
        public void ApplyTagsThrowsArgumentNullExceptionWhenIdNullOrEmpty()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = GetService();
                await service.ApplyTags(null, new string[] { "tag1", "tag2" });
            });


        }
        [Fact]
        public void ApplyTagsThrowsArgumentNullExceptionWhenTagsNullOrEmpty()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = GetService();
                await service.ApplyTags("1", null);
            });


        }
        [Fact]
        public void ApplyTagsThrowsNotFoundExceptionWhenIdNotExist()
        {
            Assert.ThrowsAsync<NotFoundException>(async () =>
            {
                var service = GetService();
                await service.ApplyTags("invalid", new string[] { "tag1", "tag2" });
            });


        }

        [Fact]
        public async void ApplyTagsWhenIdSuccess()
        {
            var service = GetService();
            await service.Send(new Message { To = "kinjal.s@sigmainfo.net", From = "kinjal.s@sigmainfo.net", Tags = new string[] { "default1" } });

            var result = await service.GetMessages();
            var firstMessage = result.First();

            await service.ApplyTags(firstMessage.Id, new string[] { "tag1", "tag2" });
            result = await service.GetMessages();
            var updatedMessage = result.First();
            Assert.NotNull(updatedMessage);
            Assert.Contains("tag1", updatedMessage.Tags);
            Assert.Contains("tag2", updatedMessage.Tags);
        }

        [Fact]
        public void RemoveTagsThrowsArgumentNullExceptionWhenIdNullOrEmpty()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = GetService();
                await service.RemoveTags(null, new string[] { "tag2" });
            });


        }
        [Fact]
        public void RemoveTagsThrowsArgumentNullExceptionWhenTagsNullOrEmpty()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = GetService();
                await service.RemoveTags("1", null);
            });


        }
        [Fact]
        public void RemoveTagsThrowsNotFoundExceptionWhenIdNotExist()
        {
            Assert.ThrowsAsync<NotFoundException>(async () =>
            {
                var service = GetService();
                await service.RemoveTags("invalid", new string[] { "tag2" });
            });


        }

        [Fact]
        public async void RemoveTagsWhenIdSuccess()
        {
            var service = GetService();
            await service.Send(new Message { To = "kinjal.s@sigmainfo.net", From = "kinjal.s@sigmainfo.net", Tags = new string[] { "tag1", "tag2" } });

            var result = await service.GetMessages();
            var firstMessage = result.First();

            await service.RemoveTags(firstMessage.Id, new string[] { "tag2" });
            result = await service.GetMessages();
            var updatedMessage = result.First();
            Assert.NotNull(updatedMessage);
            Assert.Contains("tag1", updatedMessage.Tags);
            Assert.DoesNotContain("tag2", updatedMessage.Tags);
        }

        private IInbox GetService()
        {
            var tenantService = new Mock<ITenantService>();
            tenantService.Setup(t => t.Current).Returns(new TenantInfo { Id = "tenant01" });
            var tokenInfo = new Mock<IToken>();
            tokenInfo.Setup(c => c.Subject).Returns("kinjal.s@sigmainfo.net");
            var tokenReader = new Mock<ITokenReader>();
            var tokenHandler = new Mock<ITokenHandler>();
            var token = "asfasf";
            tokenReader.Setup(r => r.Read()).Returns(token);
            tokenHandler.Setup(h => h.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s@sigmainfo.net" });
            return new Inbox(new FakeMessageRepository(), tenantService.Object, tokenHandler.Object, tokenReader.Object, Mock.Of<IEventHubClient>(), Mock.Of<ILogger>(), Mock.Of<ITenantTime>(), Mock.Of<IDecisionEngineService>(), Mock.Of<IIdentityService>(), Mock.Of<IAssignmentService>());
        }
    }
}
