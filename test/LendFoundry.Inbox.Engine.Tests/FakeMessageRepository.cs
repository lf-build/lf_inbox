﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using LendFoundry.Foundation.Persistence;
using System.Linq.Expressions;

namespace LendFoundry.Inbox.Engine.Tests
{
    public class FakeMessageRepository : IMessageRepository
    {
        public List<IMessage> messages { get; set; } = new List<IMessage>();

        void IRepository<IMessage>.Add(IMessage message)
        {

            message.Id = Guid.NewGuid().ToString();

            Task.Run(() => messages.Add(message));
        }

        public Task Add(string tenantId, IMessage message)
        {
            throw new NotImplementedException();
        }

        public Task AddMany(string tenantId, List<IMessage> messages)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IMessage>> All(Expression<Func<IMessage, bool>> query, int? skip = null, int? quantity = null)
        {
            throw new NotImplementedException();
        }

        public int Count(Expression<Func<IMessage, bool>> query)
        {
            throw new NotImplementedException();
        }

        public async Task<IMessage> Get(string username, string messageId)
        {
            if (messageId == "invalid")
                return null;
            return await Task.Run(() => messages.Where(p => p.To == username && p.Id == messageId).FirstOrDefault());
        }

        public async Task<IMessage> Get(string tenantId, string username, string messageId)
        {
            return await Task.FromResult(messages.Where(p => p.To == username).FirstOrDefault());
        }

        public Task<IMessage> Get(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<IMessage>> GetAll(string username)
        {
            return await Task.FromResult<IEnumerable<IMessage>>(messages.Where(p => p.To == username).ToList());
        }

        public Task<IEnumerable<IMessage>> GetAll(string tenantId, string username)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<IMessage>> GetAll(string tenantId, string username, bool excludeArchive = false)
        {
            return await Task.FromResult<IEnumerable<IMessage>>(messages.Where(p => p.To == username).ToList()); ;
        }

        public void Remove(IMessage item)
        {
            throw new NotImplementedException();
        }

        public async Task Update(string username, IMessage message)
        {
            messages.Remove(messages.First(p => p.To == username && p.Id == message.Id));
            await Task.Run(() => messages.Add(message));
        }

        public async Task Update(string tenantId, string username, IMessage message)
        {
            messages.Remove(messages.First(p => p.To == username && p.Id == message.Id));
            await Task.Run(() => messages.Add(message));
        }

        public void Update(IMessage item)
        {
            throw new NotImplementedException();
        }
    }
}
