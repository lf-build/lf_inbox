﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Inbox.Api.Controllers;
using LendFoundry.Inbox.Mongo;
using LendFoundry.Security.Tokens;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Inbox.Api.Tests
{
   public class ApiControllerTests
    {
        [Fact]
        public async  void SendReturnsArgumentException()
        {
            var inbox = new Mock<IInbox>();
            inbox.Setup(p => p.Send(It.IsAny<IMessage>())).Throws<ArgumentException>();
            var controller = new ApiController(inbox.Object, Mock.Of<ILogger>(),Mock.Of<IToken>());
            var response = await controller.Send(null,"kinjal.s@sigmainfo.net",new Models.SendMessageRequest { });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result?.StatusCode);
        }

        [Fact]
        public async void SendReturnsWithSuccess()
        {
            var inbox = new Mock<IInbox>();
            inbox.Setup(p => p.Send(It.IsAny<IMessage>())).Returns(Task.FromResult(true));
        
            var controller = new ApiController(inbox.Object, Mock.Of<ILogger>(), Mock.Of<IToken>());
            var response = await controller.Send("kinjal.s@sigmainfo.net", "kinjal.s@sigmainfo.net", new Models.SendMessageRequest { });
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.NotNull(result);
            Assert.Equal(204, result?.StatusCode);
        }


        [Fact]
        public async void GetMessagesReturnsWithSuccess()
        {
            var inbox = new Mock<IInbox>();
            inbox.Setup(p => p.Send(It.IsAny<IMessage>())).Throws<ArgumentException>();
            var controller = new ApiController(inbox.Object, Mock.Of<ILogger>(), Mock.Of<IToken>());
            var response = await controller.GetMessages();
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.NotNull(result);
            Assert.Equal(200, result?.StatusCode);
        }

        [Fact]
        public async void ReadReturnsArgumentException()
        {
            var inbox = new Mock<IInbox>();
            inbox.Setup(p => p.Read(It.IsAny<string>())).Throws<ArgumentException>();
            var controller = new ApiController(inbox.Object, Mock.Of<ILogger>(), Mock.Of<IToken>());
            var response = await controller.Read(null);
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result?.StatusCode);
        }

        

        [Fact]
        public async void ReadReturnsWithSuccess()
        {
            var inbox = new Mock<IInbox>();
            inbox.Setup(p => p.Read(It.IsAny<string>())).ReturnsAsync(new Message());
            var controller = new ApiController(inbox.Object, Mock.Of<ILogger>(), Mock.Of<IToken>());
            var response = await controller.Read("123");
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.NotNull(result);
            Assert.Equal(200, result?.StatusCode);
        }



        [Fact]
        public async void ArchiveReturnsArgumentException()
        {
            var inbox = new Mock<IInbox>();
            inbox.Setup(p => p.Archive(It.IsAny<string>())).Throws<ArgumentException>();
            var controller = new ApiController(inbox.Object, Mock.Of<ILogger>(), Mock.Of<IToken>());
            var response = await controller.Archive(null);
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result?.StatusCode);
        }

      

        [Fact]
        public async void ArchiveReturnsWithSuccess()
        {
            var inbox = new Mock<IInbox>();
            inbox.Setup(p => p.Archive(It.IsAny<string>())).Returns(Task.FromResult(true));
            var controller = new ApiController(inbox.Object, Mock.Of<ILogger>(), Mock.Of<IToken>());
            var response = await controller.Archive("123");
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.NotNull(result);
            Assert.Equal(204, result?.StatusCode);
        }


        [Fact]
        public async void MarkAsUnreadReturnsArgumentException()
        {
            var inbox = new Mock<IInbox>();
            inbox.Setup(p => p.MarkAsUnread(It.IsAny<string>())).Throws<ArgumentException>();
            var controller = new ApiController(inbox.Object, Mock.Of<ILogger>(), Mock.Of<IToken>());
            var response = await controller.MarkAsUnread(null);
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result?.StatusCode);
        }



        [Fact]
        public async void MarkAsUnreadReturnsWithSuccess()
        {
            var inbox = new Mock<IInbox>();
            inbox.Setup(p => p.MarkAsUnread(It.IsAny<string>())).Returns(Task.FromResult(true));
            var controller = new ApiController(inbox.Object, Mock.Of<ILogger>(), Mock.Of<IToken>());
            var response = await controller.MarkAsUnread("123");
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.NotNull(result);
            Assert.Equal(204, result?.StatusCode);
        }


        [Fact]
        public async void PinReturnsArgumentException()
        {
            var inbox = new Mock<IInbox>();
            inbox.Setup(p => p.Pin(It.IsAny<string>())).Throws<ArgumentException>();
            var controller = new ApiController(inbox.Object, Mock.Of<ILogger>(), Mock.Of<IToken>());
            var response = await controller.Pin(null);
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result?.StatusCode);
        }



        [Fact]
        public async void PinReturnsWithSuccess()
        {
            var inbox = new Mock<IInbox>();
            inbox.Setup(p => p.Pin(It.IsAny<string>())).Returns(Task.FromResult(true));
            var controller = new ApiController(inbox.Object, Mock.Of<ILogger>(), Mock.Of<IToken>());
            var response = await controller.Pin("123");
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.NotNull(result);
            Assert.Equal(204, result?.StatusCode);
        }

        [Fact]
        public async void UnPinReturnsArgumentException()
        {
            var inbox = new Mock<IInbox>();
            inbox.Setup(p => p.Unpin(It.IsAny<string>())).Throws<ArgumentException>();
            var controller = new ApiController(inbox.Object, Mock.Of<ILogger>(), Mock.Of<IToken>());
            var response = await controller.Unpin(null);
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result?.StatusCode);
        }



        [Fact]
        public async void UnPinReturnsWithSuccess()
        {
            var inbox = new Mock<IInbox>();
            inbox.Setup(p => p.Unpin(It.IsAny<string>())).Returns(Task.FromResult(true));
            var controller = new ApiController(inbox.Object, Mock.Of<ILogger>(), Mock.Of<IToken>());
            var response = await controller.Unpin("123");
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.NotNull(result);
            Assert.Equal(204, result?.StatusCode);
        }


        [Fact]
        public async void ApplyTagsReturnsArgumentException()
        {
            var inbox = new Mock<IInbox>();
            inbox.Setup(p => p.ApplyTags(It.IsAny<string>(),It.IsAny<string[]>())).Throws<ArgumentException>();
            var controller = new ApiController(inbox.Object, Mock.Of<ILogger>(), Mock.Of<IToken>());
            var response = await controller.ApplyTags(null,new string[] { "tag1"});
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result?.StatusCode);
        }



        [Fact]
        public async void ApplyTagsReturnsWithSuccess()
        {
            var inbox = new Mock<IInbox>();
            inbox.Setup(p => p.ApplyTags(It.IsAny<string>(),It.IsAny<string[]>())).Returns(Task.FromResult(true));
            var controller = new ApiController(inbox.Object, Mock.Of<ILogger>(), Mock.Of<IToken>());
            var response = await controller.ApplyTags("123",new string[] { "tag1" });
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.NotNull(result);
            Assert.Equal(204, result?.StatusCode);
        }



        [Fact]
        public async void RemoveTagsReturnsArgumentException()
        {
            var inbox = new Mock<IInbox>();
            inbox.Setup(p => p.RemoveTags(It.IsAny<string>(), It.IsAny<string[]>())).Throws<ArgumentException>();
            var controller = new ApiController(inbox.Object, Mock.Of<ILogger>(), Mock.Of<IToken>());
            var response = await controller.RemoveTags(null, new string[] { "tag1" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result?.StatusCode);
        }



        [Fact]
        public async void RemoveTagsReturnsWithSuccess()
        {
            var inbox = new Mock<IInbox>();
            inbox.Setup(p => p.RemoveTags(It.IsAny<string>(), It.IsAny<string[]>())).Returns(Task.FromResult(true));
            var controller = new ApiController(inbox.Object, Mock.Of<ILogger>(), Mock.Of<IToken>());
            var response = await controller.RemoveTags("123", new string[] { "tag1" });
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.NotNull(result);
            Assert.Equal(204, result?.StatusCode);
        }
    }
}
